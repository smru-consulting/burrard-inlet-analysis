% Script to break massive AIS file form marine traffic into month files
close all; clear all; clc

aisFolderLoc =dir('Z:\SMRU_FH\Projects\Burrard Inlet 2021\AIS\*.csv');
aisOutFoler='Z:\SMRU_FH\Projects\Burrard Inlet 2021\AISmonthified';

startMonthYear = datetime('20210101','Format', 'yyyyMMdd')
endMonthYear = startMonthYear

% Separate by month and year
for ii=1:length(aisFolderLoc)

    data= readtable(fullfile(aisFolderLoc(ii).folder,aisFolderLoc(ii).name));
    % black magic
    data.MonthYear1= fix(yyyymmdd(data.TIMESTAMPUTC)/100);
    
    % unique dateperiods
    uniqueMonthYears = unique(data.MonthYear1);
    
    
    for jj = 1:height(uniqueMonthYears)
        
        dataSub = data(data.MonthYear1==uniqueMonthYears(jj),:);
        
        outName =fullfile(aisOutFoler, ['AISout', num2str(uniqueMonthYears(jj)), '.csv']);
        
        % if the file exists, then you need to open and append
        if isfile(outName)
        disp('blarg')
        writetable(dataSub, outName, 'WriteMode','Append','WriteVariableNames',false)
        else    % file doesn't exist just write it
            writetable(dataSub, outName)
        end
        
    end
        
end