function nbits = GravitytoAccel(g)
% Function to convert 16 bit accelerometer data to gravity
% Input - accelerometer value (assumes 16 bits)


nbits = (g*32768)./2;

end