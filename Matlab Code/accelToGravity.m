function g = accelToGravity(nbits)
% Function to convert 16 bit accelerometer data to gravity
% Input - accelerometer value (assumes 16 bits)


g = (nbits*2)./32768;

end