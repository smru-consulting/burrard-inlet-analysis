function [baddata]= createMedianCSVs(files, scale, csvOutloc,depName)

% Create meian sound levels using hte processed mat file
% Inputs -
% matFileLoc - location of the daily processed sound level mat file
% from the noise analyis GUI
% scale - 'hour' or 'day'
% csvOutloc- where to store the resulting csv files

if nargin ==3
    depName ='Unknown'
end

baddata=[];


switch scale
    case 'day'
        
        if ~isstruct(files)
            files = dir(fullfile(folderLoc, '**/*.mat*'));
        end
        
        tableOut =table();
        D =regexp(files(1).name,'\d{8}','match','once')
        currentMonth = month(datetime( datenum(D,'yyyymmdd'),...
            'ConvertFrom', 'datenum'))
        
        for ii=1:length(files)
            
            D = regexp(files(ii).name,'\d{8}','match','once');
            E = datetime( datenum(D,'yyyymmdd'), 'ConvertFrom', 'datenum')
            
            newmonth = month(E);
            
            
            %If we hit a new month, export the CSV
            if newmonth ~= currentMonth
                
                csvName =['MinNoise_', files(ii).name, '_', files(ii-1).name(1:6), '.csv']
                writetable(tableOut, fullfile(csvOutloc, csvName) )
                % reset the output table and the current month
                currentMonth = newmonth;
                tableOut = table();
                
            end
            
            
            % load the mat files
            try
                load([files(ii).folder, '\', files(ii).name]);
                
                % create the table which will be kicked out
                df = table(conkA13(2:end,1),conkABROAD(2:end,2),...
                    conkA10_100(2:end,2), conkA100_1000(2:end,2),...
                    conkA10000_100000(2:end,2), conkA10000_100000(2:end,2),...
                    'Variablenames', {'MatlabTime',...
                    'Broadband', 'A10_100', 'A100_1k', 'A1k_10k', 'A10k_100k'});
                
                df.dayHr = datestr(conkA13(2:end,1), 'dd/mm/yyyy HH');
                df.dayHRmin = datestr(conkA13(2:end,1), 'dd/mm/yyyy HH:MM');
                df.Day = datestr(conkA13(2:end,1), 'dd/mm/yyyy');
                df.Location = repmat(depName, height(df), 1);
                
                % Get the hourly median values for each of the concatenated
                % values
                statTable = varfun(@median,df,'GroupingVariables','dayHr',...
                    'InputVariables',{'MatlabTime',...
                'Broadband', 'A10_100', 'A100_1k', 'A1k_10k', 'A10k_100k'});
                
                disp(['Calcaulating Daily Median for  file: ' num2str(ii) ...
                    ' of ', num2str(length(files))])
                
                tableOut = [tableOut; statTable];
            catch
                baddata = [baddata; [files(ii).folder, '\', files(ii).name]];
            end
            
            
        end
        
        csvName =['MedNoise_', files(ii).name, '_', files(ii-1).name(1:6), '.csv'];
        writetable(tableOut, fullfile(csvOutloc, csvName) )
        
        
        
        
        
    case 'hour'
        
        
        if ~isstruct(files)
            files = dir(fullfile(folderLoc, '**/*.mat*'));
        end
        
      
        D =regexp(files(1).name,'\d{8}','match','once')

        
        tableOut = table();
        %currentMonth = files(1).name(5:6);
        
        for ii=1:length(files)
            
            
            try
            % Iterate through the mat files and extract the needed variables
            
            load([files(ii).folder, '\', files(ii).name]);
            
            df = table(conkA13(2:end,1),conkABROAD(2:end,2),...
                conkA10_100(2:end,2), conkA100_1000(2:end,2),...
                conkA10000_100000(2:end,2), conkA10000_100000(2:end,2),...
                'Variablenames', {'MatlabTime',...
                'Broadband', 'A10_100', 'A100_1k', 'A1k_10k', 'A10k_100k'});
            
            df.dayHr = datestr(conkA13(2:end,1), 'dd/mm/yyyy HH');
            df.dayHRmin = datestr(conkA13(2:end,1), 'dd/mm/yyyy HH:MM');
            df.Day = datestr(conkA13(2:end,1), 'dd/mm/yyyy');
            df.Location = repmat(depName, height(df), 1)
            
            
            % Get the hourly median values for each of the concatenated
            % values
            statTable = varfun(@median,df,'GroupingVariables','dayHRmin',...
                'InputVariables',{'MatlabTime',...
                'A10_100'});
            
            csvName =['MinNoise_', files(ii).name, '.csv']
            writetable(statTable, fullfile(csvOutloc, csvName) )
            disp(['Calcaulating file: ' num2str(ii) ' of ', num2str(length(files))])
            
                        catch
                baddata = [baddata; [files(ii).folder, '\', files(ii).name]];
            end
            
            
        end
        
    

end


folders = dir(matFileloc);






end