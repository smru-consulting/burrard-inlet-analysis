
close all; clear all; clc
folderLoc = 'Z:\Projects\2019 Burrard Inlet\Noise analyses\Results';
folders = dir(folderLoc)


for ii=1:length(folders)
    
    % If the folder name is numeric, keep the file loc
    if ~isempty(regexp((folders(ii).name), '\d', 'match'))
        
        filesLoc = [folderLoc,'\', (folders(ii).name),'\'];
        files = dir(strcat(filesLoc, '*.mat'));
        tableOut = table();
        %currentMonth = files(1).name(5:6);
        
        for jj=150:length(files)
            
            
            %   newmonth = files(jj).name(5:6);
            
            
            % If we hit a new month, export the CSV
            %             if ~strcmp(newmonth, currentMonth)
            %
            %                 csvName =['MinNoise_', folders(ii).name, '_', files(jj-1).name(1:6), '.csv']
            %                 %writetable(tableOut, ['Z:\Projects\2019 Burrard Inlet\Noise analyses\Results\DailyMedianNL',...
            %                 %'\', csvName]);
            %                 writetable(tableOut, ['E:\WorkingData\BurrardNoiseMin','\', csvName] )
            %                 % reset the output table and the current month
            %                 currentMonth = newmonth;
            %                 tableOut = table();
            %
            %
            %
            %             end
            
            % Iterate through the mat files and extract the needed variables
            load([filesLoc, '\', files(jj).name]);
            
            df = table(conkA13(2:end,1),conkABROAD(2:end,2),...
                conkA10_100(2:end,2), conkA100_1000(2:end,2),...
                conkA10000_100000(2:end,2), conkA10000_100000(2:end,2),...
                'Variablenames', {'MatlabTime',...
                'Broadband', 'A10_100', 'A100_1k', 'A1k_10k', 'A10k_100k'});
            
            df.dayHr = datestr(conkA13(2:end,1), 'dd/mm/yyyy HH');
            df.dayHRmin = datestr(conkA13(2:end,1), 'dd/mm/yyyy HH:MM');
            df.Day = datestr(conkA13(2:end,1), 'dd/mm/yyyy');
            
            
            
            % Get the hourly median values for each of the concatenated
            % values
            statTable = varfun(@median,df,'GroupingVariables','dayHRmin',...
                'InputVariables',{'MatlabTime',...
                'A10_100'});
            %'Broadband', 'A10_100', 'A100_1k', 'A1k_10k', 'A10k_100k'});
            
            csvName =['MinNoise_', folders(ii).name, '_', files(jj).name, '.csv']
            writetable(statTable, ['E:\WorkingData\BurrardNoiseMin\IndianArm','\', csvName] )
            
            %tableOut = [tableOut; statTable];
            
            disp(['Calcaulating file: ' num2str(jj) ' of ', num2str(length(files))])
            
            
            
            
        end
        
        
    end
end
