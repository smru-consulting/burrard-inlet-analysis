close all; clear all; clc

% Load the data for the third octaves

% Near cab
pb306_1 = load('C:\Data\Bulker Noise\ProcessedMatfiles\pb306\306_20210922_02Chan_250kHz_10_125_ch1.mat')
pb306_2 = load('C:\Data\Bulker Noise\ProcessedMatfiles\pb306\306_20210922_02Chan_250kHz_10_125_ch2.mat')

% Far cab
pb398_1 = load('C:\Data\Bulker Noise\ProcessedMatfiles\pb398\20210922_02Chan_250kHz_10_125_ch1.mat')
pb398_2 = load('C:\Data\Bulker Noise\ProcessedMatfiles\pb398\20210922_02Chan_250kHz_10_125_ch2.mat')

% Take averages from each hydrophone
nearCab=  10*log10(10.^(pb306_1.conkA13(2:end, 2:end)./10)+10.^(pb306_2.conkA13(2:end, 2:end)./10))
farCab =  10*log10(10.^(pb398_1.conkA13(2:end, 2:end)./10)+10.^(pb398_2.conkA13(2:end, 2:end)./10))


% broadband calcs
nearCabBroadband = 10*log10(0.5*...
    (10.^(pb306_1.conkABROAD(2:end,2)./10)...
    +10.^(pb306_2.conkABROAD(2:end, 2)./10)));

farCabBroadband = 10*log10(0.5*(10.^(pb398_1.conkABROAD(2:end,2)./10)+...
    10.^(pb398_2.conkABROAD(2:end, 2)./10)));

% Decade band calcs
nearCab = [pb306_1.conkA13(2:end,1) nearCab]
nearCab = [pb306_1.conkA13(1,:); nearCab]

farCab = [pb398_1.conkA13(2:end,1) farCab]
farCab = [pb398_1.conkA13(1,:); farCab]

f_s1 = pb398_1.conkA13(1,2:end)

% Plot raw recordings
figure
subplot(2,1,1)
plot( pb306_1.conkA100_1000(2:end,1), pb306_1.conkA100_1000(2:end,2))
hold on
plot( pb306_2.conkA100_1000(2:end,1),pb306_2.conkA100_1000(2:end,2))
ylabel('Broadband SPL (dB re 1\muPa)');
title('Near CAB 100hz-1khz Received Level')
legend({'Deep', 'Shallow'})
datetick('x','HH:MM:SS')
subplot(2,1,2)
plot( pb398_1.conkA100_1000(2:end,1),pb398_1.conkA100_1000(2:end,2))
hold on
plot( pb398_1.conkA100_1000(2:end,1),pb398_2.conkA100_1000(2:end,2))
ylabel('Broadband SPL (dB re 1\muPa)');
title('Far CAB 100hz-1khz Received Level')
legend({'Deep', 'Shallow'})
datetick('x','HH:MM:SS')
ylim([110 150])
subplot(2,1,2)
ylim([110 150])

% housekeeping
%clear pb398_2 pb398_2 pb306_1 pb306_2

%% Figure out which data to include

idxFar = 95:180
idxNear = 109:194

% try start of the day instead

farTimesidx = find(pb398_1.conkA10000_100000(:,1)<datenum('22-Sep-2021 20:00:01') &...
    (pb398_1.conkA10000_100000(:,1)>datenum('22-Sep-2021 19:30:01')))

nearTimesidx = find(pb306_1.conkA10000_100000(:,1)>= min(pb398_1.conkA10000_100000(farTimesidx,1)) &...
pb306_1.conkA10000_100000(:,1)<= max(pb398_1.conkA10000_100000(farTimesidx,1)))
nearTimesidx=[nearTimesidx; max(nearTimesidx)+1]

idxFar = (farTimesidx(2:28))
idxNear = (nearTimesidx(2:28))



% only keep the good times
times = farCab((idxFar),1); % when it got bonked

% Trim the meta so you can use indexing
pb398_1 =rmfield(pb398_1, 'meta')
pb398_2 = rmfield(pb398_2, 'meta')
pb306_1 = rmfield(pb306_1, 'meta')
pb306_2 = rmfield(pb306_2, 'meta')

% select only usable data from the more limited far cab
pb398_2_trimmed = structfun(@(x) x(idxFar,:),pb398_2,'Uni',false)
pb398_1_trimmed = structfun(@(x) x(idxFar,:),pb398_1,'Uni',false)

% Figure out the indecies represented for the near cab
nearCabData= nearCab(109:194, 2:end)

% Trim the data
pb306_2_trimmed = structfun(@(x) x(idxNear,:),pb306_2,'Uni',false)
pb306_1_trimmed = structfun(@(x) x(idxNear,:),pb306_1,'Uni',false)


% Recordings to keep in the analysis
figure
subplot(2,1,1)
plot( pb306_1_trimmed.conkABROAD(2:end,1), pb306_1_trimmed.conkABROAD(2:end,2))
hold on
plot( pb306_2_trimmed.conkABROAD(2:end,1),pb306_2_trimmed.conkABROAD(2:end,2))
ylabel('Broadband SPL (dB re 1\muPa)');
title('Near CAB Broadband Received Level')
legend({'Deep', 'Shallow'})
datetick('x','HH:MM:SS')
subplot(2,1,2)
plot( pb398_1_trimmed.conkA100_1000(2:end,1),pb398_1_trimmed.conkABROAD(2:end,2))
hold on
plot( pb398_1_trimmed.conkA100_1000(2:end,1),pb398_2_trimmed.conkABROAD(2:end,2))
ylabel('Broadband SPL (dB re 1\muPa)');
title('Far CAB Broadband Received Level')
legend({'Deep', 'Shallow'})
datetick('x','HH:MM:SS')
ylim([110 150])
subplot(2,1,2)
ylim([110 150])

%% Get the location of of the cabs and calculate the distance to each
CABloc =readtable('C:\Data\Bulker Noise\streamer.csv')

CABloc= CABloc((datenum(CABloc.UTC+minutes(20))>=min(times) &...
    (datenum(CABloc.UTC-minutes(20))<=max(times))),:)

farCABloc = CABloc(CABloc.ChannelBitmap ==2,:);
nearCABloc = CABloc(CABloc.ChannelBitmap ==1,:);

NearCabLocLat = interp1(datenum(nearCABloc.UTC),...
    nearCABloc.Latitude, times, 'v5cubic')
NearCabLocLon = interp1(datenum(nearCABloc.UTC),...
    nearCABloc.Longitude, times, 'v5cubic')
FarCabLocLat = interp1(datenum(farCABloc.UTC),...
    farCABloc.Latitude, times, 'v5cubic')
FarCabLocLon = interp1(datenum(farCABloc.UTC),...
    farCABloc.Longitude, times, 'v5cubic')


NearCabLocLat(isnan(NearCabLocLat)) = NearCabLocLat(find(~isnan(NearCabLocLat), 1, 'last'))
NearCabLocLon(isnan(NearCabLocLon)) = NearCabLocLon(find(~isnan(NearCabLocLon), 1, 'last'))
FarCabLocLat(isnan(FarCabLocLat)) = FarCabLocLat(find(~isnan(FarCabLocLat), 1, 'last'))
FarCabLocLon(isnan(FarCabLocLon)) = FarCabLocLon(find(~isnan(FarCabLocLon), 1, 'last'))

%% AIS data from the ship
AisData =readtable('C:\Data\Bulker Noise\AISdata.csv');

% Vessel was the MIESTRO PEARL AIS AIS 538006340
AisData=AisData(AisData.Var11==538006340,:);
Aiskeep = table(AisData.Var3, AisData.Var21, AisData.Var22)
Aiskeep= Aiskeep(~isnan(Aiskeep.Var3),:);

% remove duplicates
[~,ia] = unique(Aiskeep.Var1)
Aiskeep= Aiskeep(ia,:);
shipLocLat = interp1(datenum(Aiskeep.Var1),...
    Aiskeep.Var2, times, 'v5cubic');
shipLocLon = interp1(datenum(Aiskeep.Var1),...
    Aiskeep.Var3, times, 'v5cubic');

% not enough AIS data, use the last observation to populate
shipLocLat(isnan(shipLocLat)) = shipLocLat(find(~isnan(shipLocLat), 1, 'last'))
shipLocLon(isnan(shipLocLon)) = shipLocLon(find(~isnan(shipLocLon), 1, 'last'))

%% Plot All location data

figure
plot(shipLocLon , shipLocLat, 'k');
hold on
scatter(FarCabLocLon, FarCabLocLat, 'r');
scatter(NearCabLocLon, NearCabLocLat, 'b');
legend({'Ship', 'Far CAB', 'Near CAB'})

plot(FarCabLocLon, FarCabLocLat, 'r');
plot(NearCabLocLon, NearCabLocLat, 'b');

% 
% figure
% scatter(Aiskeep.Var3, Aiskeep.Var2, 'k');
% hold on
% scatter(farCABloc.Longitude, farCABloc.Latitude, 'r');
% scatter(nearCABloc.Longitude, nearCABloc.Latitude, 'b');
% legend({'Ship', 'Far CAB', 'Near CAB'})



%% Start building table containing all data
% Create the table where ranges, SL, and times will be stored
rangeDF = table(times, NearCabLocLat,NearCabLocLon,...
    FarCabLocLat,FarCabLocLon,...
    shipLocLat, shipLocLon)

% range between boat and near cab
rangeDF.nearRange = vdist2(...
    rangeDF.NearCabLocLat,...
    rangeDF.NearCabLocLon,...
    rangeDF.shipLocLat,rangeDF.shipLocLon)-57;

% range between boat and far cab
rangeDF.farRange = vdist2(...
    rangeDF.FarCabLocLat,...
    rangeDF.FarCabLocLon,...
    rangeDF.shipLocLat,rangeDF.shipLocLon)-57;


% Transmission loss
rangeDF.tlNear = 18*log10(rangeDF.nearRange);
rangeDF.tlFar = 18*log10(rangeDF.farRange);


% Broadband
rangeDF.BroadNear = 10*log10(0.5*(10.^(pb306_1_trimmed.conkABROAD(:,2)./10)+...
    10.^(pb306_2_trimmed.conkABROAD(:, 2)./10)));

rangeDF.BroadFar = 10*log10(0.5*(10.^(pb398_1_trimmed.conkABROAD(:,2)./10)+...
    10.^(pb398_2_trimmed.conkABROAD(:, 2)./10)));

% Third Ocatave
rangeDF.thridNear = 10*log10(0.5*(10.^(pb306_1_trimmed.conkA13(:,2:end)./10)+...
    10.^(pb306_2_trimmed.conkA13(:, 2:end)./10)));

rangeDF.thirdFar = 10*log10(0.5*(10.^(pb398_1_trimmed.conkA13(:,2:end)./10)+...
    10.^(pb398_2_trimmed.conkA13(:, 2:end)./10)));

% jUST TESTING
rangeDF.thirdFar = pb398_1_trimmed.conkA13(:,2:end);


%% Source level estimates


% Pull out a distinct value or two for plotting


% Ansi equation 3
deltaL = rangeDF.thridNear-rangeDF.thirdFar

% ansi equation 4
Lprime = abs(10*log10(10.^(rangeDF.thridNear/10)-...
    10.^(rangeDF.thirdFar/10)));

SL_cvessel_f =Lprime+(18*log10(mean(rangeDF.nearRange)))

figure
% More intuitive way
% NL = rangeDF.thridNear-rangeDF.thirdFar;
% SL_cvessel_f = rangeDF.thridNear-NL+18*log10(rangeDF.nearRange);
% % Third octave vessel source level
% SL_cvessel = abs(10*log10(10.^(rangeDF.thridNear./10) - 10.^(rangeDF.thirdFar./10)));
% SL_cvessel_f = SL_cvessel +  rangeDF.tlNear; %TL needs to be added after background noise is subtracted from signal+noise (dt Oct 16th 2019)
% 

% adjust very low and very high SNR values
% rangeDF.SNR = deltaL;
% SL_cvessel_f(rangeDF.SNR<10)=nan;
% tl = 18*log10(repmat(rangeDF.nearRange, [1, size(rangeDF.thridNear,2)]));
%  SL_cvessel_f(rangeDF.SNR>10)= rangeDF.thridNear(rangeDF.SNR>10)+...
%      tl(rangeDF.SNR>10);

rangeDiff = rangeDF.tlFar-rangeDF.tlNear


figure
stairs(f_s1,median(SL_cvessel_f, 'omitnan'));
set(gca,'XScale','log');
xlabel('Frequency [ Hz ]');
ylabel('1/3 Octave Band SPL (dB re 1\muPa)');
legend('near CAB','far CAB');
title('Bulker at Anchor');


clear CABloc range ia
