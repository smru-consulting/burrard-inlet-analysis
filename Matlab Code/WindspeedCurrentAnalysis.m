

close all; clear all; clc

%% Location of the datafiles

% Daily median noise levels produce using the createMedianCSVs.m
dailyNLcsvloc = 'C:\Data\BurrardInlet2020\DailyMedNL\Burrard East';

% Daily rain/ winspeed
rainWndspd = readtable('C:\Data\BurrardInlet2020\Climate_daily_EC_Vancouver.csv')

% create a bit csv of the noise levels in the highest decade band
BEcsvs = dir(fullfile(dailyNLcsvloc, '*.csv'))

nlDatout = [];

for ii =1:length(BEcsvs)
    
    nlDatout = [nlDatout;...
        readtable(fullfile(BEcsvs(ii).folder, BEcsvs(ii).name), 'Delimiter', ',')]
    
end

nlDatout.Properties.VariableNames{1}='Date'




%% link nl dates to windspeeds
T = join(nlDatout,rainWndspd,'Keys','Date')


[edgesX2,edgesY2,N] = ndhist(T.AvgHourlyWindSpeed,T.median_A1k_10k,'intbins','bins' ,2 )

myColorMap = parula(100);
myColorMap(1,:) = 1;
colormap(myColorMap);

s = pcolor(edgesX2,edgesY2,N)
xlabel('Averaged Daily Windspeed (km/hr)')
ylabel('Sound Pressure Level (dB re: 1 \muPa) 1-10 khz Bin')
set(s, 'EdgeColor', 'none');
title('Sound Pressure Level vs Windspeed at Burrard East',...
    'fontweight','bold','fontsize',12)

% rain
figure
[edgesX2,edgesY2,N] = ndhist(T.Precipitation,T.median_A1k_10k,'intbins','bins' ,2 )

myColorMap = parula(100);
myColorMap(1,:) = 1;
colormap(myColorMap);

s = pcolor(edgesX2,edgesY2,N)
xlabel('Rain (mm)')
ylabel('Sound Pressure Level (dB re: 1 \muPa) 1-10 khz Bin')
set(s, 'EdgeColor', 'none');
title('Sound Pressure Level vs Rain at Burrard East',...
    'fontweight','bold','fontsize',12)
