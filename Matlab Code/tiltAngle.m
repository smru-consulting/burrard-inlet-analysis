function [phi, theta, psi] = tiltAngle(x,y,z)
% calculate the angle of the sensor relative to the ground in degrees
% https://www.digikey.com/en/articles/techzone/2011/may/using-an-accelerometer-for-inclination-sensing

x = accelToGravity(x);
y = accelToGravity(y);
z = accelToGravity(z);

%phi = acosd(z./sqrt(x.^2+y.^2+z.^2));

% soundtrap x axis == digikey z axis
phi = atand(sqrt(y.^2+z.^2)./x.^2);

theta = atand(sqrt(z.^2./(x.^2+y.^2)));
psi = atand(x.^2./sqrt(z.^2+y.^2));

end