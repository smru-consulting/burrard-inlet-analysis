close all; clear all; clc


% English Bay locations
matFileloc = [{'Z:\Projects\2020 Burrard Inlet\Dep01\Noise Analysis Output\671113255'},...
    {'Z:\Projects\2020 Burrard Inlet\Dep02\Noise Analysis Output\671129639\2020'},...
    {'Z:\Projects\2020 Burrard Inlet\Dep03\Noise Analysis Output\671129639\2020'},...
    {'Z:\Projects\2020 Burrard Inlet\Dep04\Noise Analysis Output\671129639'}];

files=[];
for ii=1:length(matFileloc)
    files = [files; dir(fullfile(matFileloc{ii}, '**/*.mat*'))]
end

csvOutLoc ='Z:\Projects\2020 Burrard Inlet\Final Anlysis\Aggregated CSVs\Daily\English Bay'
createMedianCSVs(files, 'day', csvOutLoc, 'English Bay')


%% Burrard East
% English Bay locations
matFileloc = [{'Z:\Projects\2020 Burrard Inlet\Dep01\Noise Analysis Output\671129639'},...
    {'Z:\Projects\2020 Burrard Inlet\Dep02\Noise Analysis Output\671113255'},...
    {'Z:\Projects\2020 Burrard Inlet\Dep03\Noise Analysis Output\5039\2020'},...
    {'Z:\Projects\2020 Burrard Inlet\Dep04\Noise Analysis Output\671113255'}];

files=[];
for ii=1:length(matFileloc)
    files = [files; dir(fullfile(matFileloc{ii}, '**/*.mat*'))]
end

csvOutLoc ='Z:\Projects\2020 Burrard Inlet\Final Anlysis\Aggregated CSVs\Daily\Burrard East'
createMedianCSVs(files, 'day', csvOutLoc, 'Burrard East')

%% Hourly English Bay
close all; clear all; clc


% English Bay locations
matFileloc = [{'Z:\Projects\2020 Burrard Inlet\Dep01\Noise Analysis Output\671113255'},...
    {'Z:\Projects\2020 Burrard Inlet\Dep02\Noise Analysis Output\671129639\2020'},...
    {'Z:\Projects\2020 Burrard Inlet\Dep03\Noise Analysis Output\671129639\2020'},...
    {'Z:\Projects\2020 Burrard Inlet\Dep04\Noise Analysis Output\671129639'}];

files=[];
for ii=1:length(matFileloc)
    files = [files; dir(fullfile(matFileloc{ii}, '**/*.mat*'))]
end

csvOutLoc ='Z:\Projects\2020 Burrard Inlet\Final Anlysis\Aggregated CSVs\Hourly\English Bay'
createMedianCSVs(files, 'hour', csvOutLoc, 'English Bay')

%% Hourly Burrard East
% Burrard East locations
matFileloc = [{'Z:\Projects\2020 Burrard Inlet\Dep01\Noise Analysis Output\671129639'},...
    {'Z:\Projects\2020 Burrard Inlet\Dep02\Noise Analysis Output\671113255'},...
    {'Z:\Projects\2020 Burrard Inlet\Dep03\Noise Analysis Output\5039\2020'},...
    {'Z:\Projects\2020 Burrard Inlet\Dep04\Noise Analysis Output\671113255'}];

files=[];
for ii=1:length(matFileloc)
    files = [files; dir(fullfile(matFileloc{ii}, '**/*.mat*'))]
end

csvOutLoc ='Z:\Projects\2020 Burrard Inlet\Final Anlysis\Aggregated CSVs\Hourly\Burrard East'
createMedianCSVs(files, 'hour', csvOutLoc, 'Burrard East')
