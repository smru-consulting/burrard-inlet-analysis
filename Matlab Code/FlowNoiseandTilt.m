
close all; clear all; clc

accelLoc = dir('C:\Data\BurrardInlet2020\Accelerometer\Accelerometer');


% Step through each folder
accelOut=[];
for ii = 3:length(accelLoc)


    
        accelFiles = dir(fullfile(accelLoc(ii).folder, accelLoc(ii).name, '*.csv'));
        
        for jj = 1:length(accelFiles)
            accel = readtable(fullfile(accelFiles(jj).folder, accelFiles(jj).name));
            
            accel=varfun(@mean,accel,'InputVariables',accel.Properties.VariableNames);
            accel.Dep =repmat((ii-1), height(accel),1);
              
            accelOut = [accelOut;accel];
           
            
        end
        
   

end

%%
accelOut.Date= datetime(accelOut.mean_unixTime,'ConvertFrom','epochtime');
accelOut.DateHour = accelOut.Date;
accelOut.DateHour.Format ='dd/MM/uuuu HH:mm';
accelOut.DateHour.Second=0;

[phi, theta, psi] = tiltAngle(accelOut.mean_X,...
    accelOut.mean_Y,accelOut.mean_Z);
figure;
subplot(3,1,1)
plot(phi)
subplot(3,1,2)
plot(theta)
subplot(3,1,3)
plot(psi)


thetaNorm = theta;
psiNorm = psi;
phiNorm = phi;

% Normalize for each deployment
deps = unique(accelOut.Dep)

for ii= 1:length(deps)

    dataidx = find(accelOut.Dep == deps(ii)) ;
    
    normalizedtheta = (theta(dataidx))-median(theta(dataidx(1000:end-1000)));
    thetaNorm(dataidx)=normalizedtheta;
    
    normalizedtphi = (phi(dataidx))-median(phi(dataidx(1000:end-1000)));
    psiNorm(dataidx)=normalizedtphi;
    
    normalizedtphi = (psi(dataidx))-median(psi(dataidx(1000:end-1000)));
    phiNorm(dataidx)=normalizedtphi;
    
end

accelOut.theta =thetaNorm;
accelOut.phi = phiNorm;
accelOut.psi = psiNorm; 

figure;
subplot(3,1,1)
plot(phiNorm)
subplot(3,1,2)
plot(thetaNorm)
subplot(3,1,3)
plot(psiNorm)

%% Add the hourly noise level data


nlLoc = 'C:\Data\BurrardInlet2020\Hourly\Burrard East'

% create a bit csv of the noise levels in the highest decade band
BEcsvs = dir(fullfile(nlLoc, '*.csv'))

nlDatout = [];
baddata =[];
for ii =1:length(BEcsvs)
    try
    nlDatout = [nlDatout;...
        readtable(fullfile(BEcsvs(ii).folder, BEcsvs(ii).name), 'Delimiter', ',')];
    catch
        baddata = [baddata; fullfile(BEcsvs(ii).folder, BEcsvs(ii).name)];
    end
end

nlDatout.Properties.VariableNames{1}='DateHour';
nlDatout.DateHour.Format ='dd/MM/uuuu HH:mm';
nlDatout.DateHour.Second=0;
nlDatout.DateString  = datestr(nlDatout.DateHour);
accelOut.DateString  = datestr(accelOut.DateHour);


T = outerjoin(accelOut(:, [9:11,8]), nlDatout(:,4:5),'Keys','DateString');

%%

% rain
figure
[edgesX2,edgesY2,N] = ndhist(abs(T.theta),T.median_A10_100,'intbins','bins' ,3 )

myColorMap = parula(100);
myColorMap(1,:) = 1;
colormap(myColorMap);

s = pcolor(edgesX2,edgesY2,N)
xlabel('Hydrophone Tilt Angle in Degrees')
ylabel('Sound Pressure Level (dB re: 1 \muPa) 10-100 hz Bin')
set(s, 'EdgeColor', 'none');
title('Sound Pressure Level vs Tilt Angle at Burrard East',...
    'fontweight','bold','fontsize',12)
