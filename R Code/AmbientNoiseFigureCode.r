

# Load the noise level data
olderLoc = 'Z:\\Projects\\2019 Burrard Inlet\\Noise analyses\\Results\\MedHrlyNoise_470065216_201901.csv'
nlFolderLoc = 'C:\\Users\\Kaitlin Palmer\\Box\\SMRUC NA\\Client Projects\\SMRU Canada\\Active\\PMV\\2019 Burrard Inlet PAM\\Deliverables\\Ambient Noise Results\\DailyMedianNL'
nlFolderLoc = 'C:\\Data\\burrardinlet\\2020'




files = list.files(nlFolderLoc)
nlOut = data.frame()

for (jj in 1:length(files)){
  
  depLoc = nlFolderLoc[jj]
  depFoler = paste(nlFolderLoc, files[ii], sep = '\\')
  depFiles = list.files(depFoler)

for (ii in 1:length(depFiles)){

  # Remove the chaff
  nldf = read.csv(paste(depFoler, depFiles[ii], sep = '\\'))
  
  # Add useful bits
  nldf$Day = floor(nldf$median_MatlabTime)
  nldf$Location = jj
  
  nlTemp = nldf[,c(1,3:9)]
  
  for (kk in 1:5){
    kk = nlTemp[, c('Day', colnames(nlTemp)[kk+2], colnames(nlTemp)[8])]
    colnames(kk)[2]= 'NLValue'
    kk$NLMetric = as.factor(colnames(nlTemp)[kk+2])
    
    
    nlOut= rbind(nlOut, kk)


    
  }
  

}

metaDF$DTNames2[5] = (unique(nlOut$Location))[2]

# Merge the meta data with the AIS output and calculate the daily density
nlMerged = merge(nlOut, metaDF, by.x = 'Location', by.y= 'DTNames2', all.x=TRUE)
#levels(nlMerged$JWNames) = c('Cruise Terminal', 'Westterm/Centerm',
#                             'Burrard E', 'Indian Arm', 'English Bay')

nlMerged$NamesOut <- ordered(nlMerged$NamesOut, levels(nlMerged$NamesOut)[c(2,3,1,5,4)])


nlMerged$NLMetric= as.factor(nlMerged$NLMetric)
levels(nlMerged$NLMetric) = c('Broadband',
                              '10 Hz-100 Hz',
                              '100 Hz-1 kHz',
                              '1 kHz-10 kHz',
                              '10 kHz- 100 kHz')

  
library(ggthemes)
ggplot(data = nlMerged) + 
  geom_density(aes(x = NLValue, 
                   #color = NamesOut,
                   fill= NamesOut),alpha =.5) +
  facet_grid(~NLMetric)+
  scale_fill_manual(values = rhg_cols)+
  theme_bw()+
  theme(legend.title=element_blank())+
  ylab('Probability Distribution')+
  xlab(expression('Sound Pressure Level (dB re: 1 '*mu*"Pa)"))+
  theme(text = element_text(size=13))
  







NLdf = read.csv(nlFolderLoc, header = TRUE)
NLdf$Location = unique(aisdf$Location)[1]
NLdf$Day = floor(NLdf$median_MatlabTime)
NLdf$Hour = floor(24*(NLdf$median_MatlabTime-floor(NLdf$median_MatlabTime)))


# Trim the data to periods of coverage for both
startMin = max(min(aisdf$MatlabDate), min(NLdf$median_MatlabTime))
endMin = min(max(aisdf$MatlabDate), max(NLdf$median_MatlabTime))

aisdf= aisdf[aisdf$MatlabDate>=startMin & aisdf$MatlabDate<=endMin,]
NLdf= NLdf[NLdf$median_MatlabTime>=startMin & NLdf$median_MatlabTime<=endMin,]

# Create a dataframe that for each hour has the number of boats and the median noise level
# for each 

# Aggregate the number of boats on a per day basis
aisDayAgg  = aggregate(data = aisdf, FID_2019_1~Day+Hour+Location,
                       FUN = function(x)  length(unique(x)))

aisDayAgg.Moving  = aggregate(data = aisdf,
                              FID_2019_1~Day+Hour+Location,
                       FUN = function(x)  length(unique(x)))

aisDayAgg.Moving$MedSpeed=aggregate(data = aisdf,
                              SPEED_KNOT~Day+Hour+Location,
                              FUN =  mean)[,4]

aisDayAgg.VesType = aggregate(data = aisdf,
                              FID_2019_1~Day+Hour+Location+VESSELTYPE,
                              FUN = function(x)  length(unique(x)))

aisDayAgg.VesType$MedSpeed=aggregate(data = subset(aisdf),
                                    Speed~Day+Hour+Location+VESSELTYPE,
                                    FUN =  mean)[,5]

aisDayAgg.Status = aggregate(data = aisdf,
                              FID_2019_1~Day+Hour+Location+STATUS+VESSELTYPE,
                              FUN = function(x)  length(unique(x)))

aisDayAgg.Status$MedSpeed=aggregate(data = aisdf,
                                     Speed~Day+Hour+Location+STATUS+VESSELTYPE,
                                     FUN =  mean)[,6]



nldfMerge = merge(aisDayAgg, NLdf, by = c('Day', 'Hour'))
nldfMerge.Moving = merge(aisDayAgg.Moving, NLdf, by = c('Day', 'Hour'))
nldfMerge.VesType = merge(aisDayAgg.VesType, NLdf, by = c('Day', 'Hour'))
nldfMerge.Status = merge(aisDayAgg.Status, NLdf, by = c('Day', 'Hour'))


# Do some exploratory plotting

library(ggplot2)
library(viridis)
ggplot(data = nldfMerge) + 
  geom_point(aes(FID_2019_1, median_A10_100))+
  geom_point(aes(FID_2019_1, median_A100_1k)) +
  geom_point(aes(FID_2019_1, median_A10k_100k))
  
  
  ggplot(data = nldfMerge.Moving) + 
  geom_point(aes(FID_2019_1, median_A10_100, color =log10(MedSpeed/10)))+
  scale_color_viridis()
  
  
  ggplot(data = nldfMerge.Moving) + 
    geom_point(aes((MedSpeed), median_A100_1k, color=log10(FID_2019_1)))+
  scale_color_viridis()
  
  

ggplot(data = nldfMerge.VesType) + 
  geom_point(aes((MedSpeed), (median_A10_100), color=VESSELTYPE))


ggplot(data = nldfMerge.VesType) + 
  geom_point(aes((FID_2019_1), median_A10_100, color=log10(MedSpeed)))

ggplot(data = nldfMerge.VesType) + 
  geom_point(aes((MedSpeed), median_A10_100, color=VESSELTYPE))


nldfMerge.Status$STATUS = as.factor(nldfMerge.Status$STATUS)

ggplot(data = nldfMerge.Status) + 
  geom_point(aes((FID_2019_1), median_A10_100, color= log10(MedSpeed)))

