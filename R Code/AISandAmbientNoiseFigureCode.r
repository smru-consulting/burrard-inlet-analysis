

rm(list = ls())


# Load the AIS data and calculate daily density of shipe
aisFolderLoc = 'C:\\Users\\Kaitlin Palmer\\Box\\SMRUC NA\\Client Projects\\SMRU Canada\\Active\\PMV\\2019 Burrard Inlet PAM\\Deliverables\\purchased AIS data\\Processed AIS\\'
aisFolderLoc = 'D:\\data\\SMRU_consultingTemp\\Processed AIS\\'


files = list.files(aisFolderLoc)
aisOut = data.frame()
vessleHrs = data.frame()
rhg_cols <- c("blue", "red", "black","yellow", "green")
for (ii in 1:length(files)){
  
  
  # Remove the chaff
  aisdf = read.csv(paste(aisFolderLoc, files[ii], sep = '\\'))
  
  
  # Remove likely quiet boats
  aisdf = aisdf[aisdf$STATUS !=1,] # at anchor
  aisdf = aisdf[aisdf$STATUS !=2,] # not under command
  aisdf = aisdf[aisdf$STATUS !=5,] # moored
  aisdf = aisdf[aisdf$STATUS !=6,] # grounded
  aisdf = aisdf[aisdf$STATUS !=8,] # underway sailing
  

  # Add useful bits
  aisdf$Day = floor(aisdf$MatlabDate)
  aisdf$Hour = floor(24*(aisdf$MatlabDate-floor(aisdf$MatlabDate)))
  aisdf$Speed = aisdf$SPEED_KNOT/10
  colnames(aisdf)[1] = 'BoatID'
  
  
  # Remove vessles moving <1kt
  #aisdf = aisdf[aisdf$Speed>1,]
  
  # Calculate daily sum of remaining data
  aisDayAgg  = aggregate(data = aisdf, BoatID~Day+Location,
                         FUN = function(x)  length(unique(x)))
  aisDayAgg$Month  = as.numeric(strsplit(files[ii], '_')[[1]][2])
  aisOut= rbind(aisOut, aisDayAgg)
  
  
  # Calculate the daily vessle hours
  boatIDs = unique(aisdf$SHIPNAME)
  
  # Calculate the vessle hours metric
  for (jj in 1:length(boatIDs)){
    
    # Pull out the boats
    boat = aisdf[aisdf$SHIPNAME ==boatIDs[jj],]
    
    # Pull out the hydrophones
    hyds = unique(boat$Location)
    
    for (kk in 1:length(hyds)){
      
      # One boat, one location, one month
      boatHyd = boat[boat$Location== hyds[kk],]
      
      # Sort by date
      boatHyd = boatHyd[order(boatHyd$MatlabDate),]
      
      if (nrow(boatHyd)>1){
      # Calculate the time difference
      boatHyd$diffTime = 
        c( boatHyd$MatlabDate[2:nrow(boatHyd)],boatHyd$MatlabDate[NROW(boatHyd)])-
        boatHyd$MatlabDate
      # Convert to minutes
      boatHyd$diffTime = boatHyd$diffTime*24*60
      
      boatHyd$NewGroup = ifelse(boatHyd$diffTime>6, 0,1)
      
      # Where there is a new group, the diff time should be zero
      boatHyd$TimDur = boatHyd$NewGroup* boatHyd$diffTime
      
      # Aggregate the time as a function of dates
      out= aggregate(data= boatHyd, TimDur~Day, FUN=sum)
      }else{
        out = boatHyd[c('Day', 'Location')]
        out$TimDur = 1
      }
      
      out$Location = unique(boat$Location)[kk]
      vessleHrs = rbind(vessleHrs, out)
      
      
    }
    
    
    
  }
  
  
  
  
  rm(aisdf, aisDayAgg)
}



## Calculate daily density of ships ##
# data fram for densities, JW naming scheme and DT naming scheme
JWNames = levels(aisOut$Location)
DTnames = c(470065216, 47302784, 671109159, 671113255, 671129639)
DTnames = DTnames[c(3,1,5,4,2)]
Area = c(11.76,11.74,8.6,10.59,27.89)
metaDF= data.frame(JWNames = JWNames, DTNames = DTnames, Area = Area)
metaDF$DTNames2 = metaDF$DTNames
metaDF$NamesOut = as.factor(c('Burrard-W1', 
                              'Burrard-W2', 
                              'Burrard-E', 
                              'Indian Arm', 'Eng. Bay'))

# Merge the meta data with the AIS output and calculate the daily density
aisMerged = merge(aisOut, metaDF, by.x = 'Location', by.y= 'JWNames', all.x=TRUE)
aisMerged$ShipDen = aisMerged$BoatID/aisMerged$Area
aisMerged$NamesOut <- ordered(aisMerged$NamesOut, levels(aisMerged$NamesOut)[c(2,3,1,5,4)])

tmp = aggregate(data = aisMerged, ShipDen~NamesOut, FUN = median)
tmp$ShipDen = round(tmp$ShipDen)

library(ggplot2)
ggplot(data= aisMerged, aes(NamesOut,  ShipDen,fill = NamesOut) ) + 
  geom_violin()+
  scale_fill_manual(values = rhg_cols)+
  geom_text(data=tmp, 
            aes(x= NamesOut, y = max(aisMerged$ShipDen), 
                label = ShipDen), 
            position=position_dodge(width = 0.6),
            size = 3, vjust = -0.5,colour="black")+
  theme_bw()+
  xlab('')+ theme(legend.position = "none")+
  ylab('Ship Density (AIS IDs/Day)')+
  ggtitle('Daily Vessel Density by Location Vessles Moving >1 kt/hr')+
  theme(plot.title = element_text(hjust = 0.5)) 


ggplot(data= vessleHrs, aes(Location,  TimDur, fill = Location) ) + 
  geom_violin()+
  scale_fill_manual(values = rhg_cols)

# Load the noise level data
nlF
olderLoc = 'Z:\\Projects\\2019 Burrard Inlet\\Noise analyses\\Results\\MedHrlyNoise_470065216_201901.csv'
nlFolderLoc = 'C:\\Users\\Kaitlin Palmer\\Box\\SMRUC NA\\Client Projects\\SMRU Canada\\Active\\PMV\\2019 Burrard Inlet PAM\\Deliverables\\Ambient Noise Results\\DailyMedianNL'

files = list.files(nlFolderLoc)
nlOut = data.frame()

for (ii in 1:length(files)){

  # Remove the chaff
  nldf = read.csv(paste(nlFolderLoc, files[ii], sep = '\\'))
  
  # Add useful bits
  nldf$Day = floor(nldf$median_MatlabTime)
  nldf$Location = as.numeric(substr(files[ii], 14,22))
  
  nlTemp = nldf[,c(1,3:9)]
  
  for (jj in 1:5){
    kk = nlTemp[, c('Day', colnames(nlTemp)[jj+2], colnames(nlTemp)[8])]
    colnames(kk)[2]= 'NLValue'
    kk$NLMetric = as.factor(colnames(nlTemp)[jj+2])
    
    
    nlOut= rbind(nlOut, kk)


    
  }
  

}

metaDF$DTNames2[5] = (unique(nlOut$Location))[2]

# Merge the meta data with the AIS output and calculate the daily density
nlMerged = merge(nlOut, metaDF, by.x = 'Location', by.y= 'DTNames2', all.x=TRUE)
#levels(nlMerged$JWNames) = c('Cruise Terminal', 'Westterm/Centerm',
#                             'Burrard E', 'Indian Arm', 'English Bay')

nlMerged$NamesOut <- ordered(nlMerged$NamesOut, levels(nlMerged$NamesOut)[c(2,3,1,5,4)])


nlMerged$NLMetric= as.factor(nlMerged$NLMetric)
levels(nlMerged$NLMetric) = c('Broadband',
                              '10 Hz-100 Hz',
                              '100 Hz-1 kHz',
                              '1 kHz-10 kHz',
                              '10 kHz- 100 kHz')

  
library(ggthemes)
ggplot(data = nlMerged) + 
  geom_density(aes(x = NLValue, 
                   #color = NamesOut,
                   fill= NamesOut),alpha =.5) +
  facet_grid(~NLMetric)+
  scale_fill_manual(values = rhg_cols)+
  theme_bw()+
  theme(legend.title=element_blank())+
  ylab('Probability Distribution')+
  xlab(expression('Sound Pressure Level (dB re: 1 '*mu*"Pa)"))+
  theme(text = element_text(size=13))
  







NLdf = read.csv(nlFolderLoc, header = TRUE)
NLdf$Location = unique(aisdf$Location)[1]
NLdf$Day = floor(NLdf$median_MatlabTime)
NLdf$Hour = floor(24*(NLdf$median_MatlabTime-floor(NLdf$median_MatlabTime)))


# Trim the data to periods of coverage for both
startMin = max(min(aisdf$MatlabDate), min(NLdf$median_MatlabTime))
endMin = min(max(aisdf$MatlabDate), max(NLdf$median_MatlabTime))

aisdf= aisdf[aisdf$MatlabDate>=startMin & aisdf$MatlabDate<=endMin,]
NLdf= NLdf[NLdf$median_MatlabTime>=startMin & NLdf$median_MatlabTime<=endMin,]

# Create a dataframe that for each hour has the number of boats and the median noise level
# for each 

# Aggregate the number of boats on a per day basis
aisDayAgg  = aggregate(data = aisdf, FID_2019_1~Day+Hour+Location,
                       FUN = function(x)  length(unique(x)))

aisDayAgg.Moving  = aggregate(data = aisdf,
                              FID_2019_1~Day+Hour+Location,
                       FUN = function(x)  length(unique(x)))

aisDayAgg.Moving$MedSpeed=aggregate(data = aisdf,
                              SPEED_KNOT~Day+Hour+Location,
                              FUN =  mean)[,4]

aisDayAgg.VesType = aggregate(data = aisdf,
                              FID_2019_1~Day+Hour+Location+VESSELTYPE,
                              FUN = function(x)  length(unique(x)))

aisDayAgg.VesType$MedSpeed=aggregate(data = subset(aisdf),
                                    Speed~Day+Hour+Location+VESSELTYPE,
                                    FUN =  mean)[,5]

aisDayAgg.Status = aggregate(data = aisdf,
                              FID_2019_1~Day+Hour+Location+STATUS+VESSELTYPE,
                              FUN = function(x)  length(unique(x)))

aisDayAgg.Status$MedSpeed=aggregate(data = aisdf,
                                     Speed~Day+Hour+Location+STATUS+VESSELTYPE,
                                     FUN =  mean)[,6]



nldfMerge = merge(aisDayAgg, NLdf, by = c('Day', 'Hour'))
nldfMerge.Moving = merge(aisDayAgg.Moving, NLdf, by = c('Day', 'Hour'))
nldfMerge.VesType = merge(aisDayAgg.VesType, NLdf, by = c('Day', 'Hour'))
nldfMerge.Status = merge(aisDayAgg.Status, NLdf, by = c('Day', 'Hour'))


# Do some exploratory plotting

library(ggplot2)
library(viridis)
ggplot(data = nldfMerge) + 
  geom_point(aes(FID_2019_1, median_A10_100))+
  geom_point(aes(FID_2019_1, median_A100_1k)) +
  geom_point(aes(FID_2019_1, median_A10k_100k))
  
  
  ggplot(data = nldfMerge.Moving) + 
  geom_point(aes(FID_2019_1, median_A10_100, color =log10(MedSpeed/10)))+
  scale_color_viridis()
  
  
  ggplot(data = nldfMerge.Moving) + 
    geom_point(aes((MedSpeed), median_A100_1k, color=log10(FID_2019_1)))+
  scale_color_viridis()
  
  

ggplot(data = nldfMerge.VesType) + 
  geom_point(aes((MedSpeed), (median_A10_100), color=VESSELTYPE))


ggplot(data = nldfMerge.VesType) + 
  geom_point(aes((FID_2019_1), median_A10_100, color=log10(MedSpeed)))

ggplot(data = nldfMerge.VesType) + 
  geom_point(aes((MedSpeed), median_A10_100, color=VESSELTYPE))


nldfMerge.Status$STATUS = as.factor(nldfMerge.Status$STATUS)

ggplot(data = nldfMerge.Status) + 
  geom_point(aes((FID_2019_1), median_A10_100, color= log10(MedSpeed)))

