
library(reshape2)
rm(list = ls())


# Load the AIS data and calculate daily density of shipe
aisFolderLoc = 'C:\\Data\\BurrardInlet2020\\AIS\\2019\\Processed AIS'


files = list.files(aisFolderLoc)
aisOut = data.frame()
for (ii in 1:length(files)){
  
  
  # Remove the chaff
  aisdf = read.csv(paste(aisFolderLoc, files[ii], sep = '\\'))
  aisdf$Location = as.factor( aisdf$Location)
  
  # Add useful bits
  aisdf$Day = floor(aisdf$MatlabDate)
  aisdf$Hour = floor(24*(aisdf$MatlabDate-floor(aisdf$MatlabDate)))
  aisdf$Speed = aisdf$SPEED_KNOT/10
  
  colnames(aisdf)[1] = 'BoatID'
  
  # keep only cruise ships 
  aisCruise = aisdf[aisdf$VESSELTYPE=='Passenger Ship' & 
                      aisdf$Location=='1) Burrard W (Cruise terminal)' &
                      aisdf$IMO != 9688180 &
                      aisdf$IMO != 9585003 & 
                      aisdf$IMO != 7530054 &
                      aisdf$IMO != 9139373 &
                      aisdf$IMO != 9586033 &
                      aisdf$IMO != 9586112 &
                      aisdf$IMO != 8663858 &
                      aisdf$IMO != 8120650 &
                      aisdf$IMO != 9586100,]
  
  if (nrow(aisCruise)>0){
    aisDayAgg = aisCruise[!duplicated(
      aisCruise[,c('Location', 'SHIPNAME','Day','Hour')]),]
    
    aisDayAgg$Month  = as.numeric(strsplit(files[ii], '_')[[1]][2])
    aisOut= rbind(aisOut, aisDayAgg)}
  
  
}
rm( 'aisCruise', 'aisDayAgg', 'aisdf')

# Determine which days had a Princess Cruise Present
aisOut$PRINCESSCruise =0
aisOut$PRINCESSCruise[grep("PRINCESS", aisOut$SHIPNAME)]= 1


# Vessel that was measured
aisMeasuredCruiseship = aisOut[aisOut$Day==737559,]

# Pull out the dates for comparisons the previous year
aisKeep = aisOut[aisOut$Day>=737669 & aisOut$Day<= 737740,]


# Number of cruise ships present at each our
nShipsPresent = aggregate(data = aisKeep,
                          SHIPNAME ~Day+Hour, FUN = function(x) length(unique(x)))
nShipsPresent = merge(nShipsPresent,
                      aggregate(data = aisKeep,
                                PRINCESSCruise ~Day+Hour, FUN = max),
                      by =c('Day', 'Hour'))
colnames(nShipsPresent)[3]<-'nShips'


######################################################
## Load the noise level data ##
######################################################
nlFolderLoc='D:\\2019CruiseShipSummaryCSV'

nlFolders = list.files(nlFolderLoc)
nlOut = data.frame()

for (kk in 1:66){
  
  temp = paste(nlFolderLoc, 
               nlFolders[kk], sep = '\\')
  
  # Remove the chaff
  nldf = read.csv(temp)
  
  nldf$Hour=(do.call("rbind", strsplit(as.character(nldf$dayHr),
                                       " ", fixed = TRUE)))[,2]
  # Add useful bits
  nldf$Day = floor(nldf$MatlabTime)
  
  mdata <- melt(nldf[,c(3:14)], id=c("Day","MatlabTime","Hour"))
  mdata$Location =  as.factor("Burrard-W1")
  nlOut= rbind(nlOut, mdata)
  
  
}
rm( 'mdata', 'nldf', 'temp')

# Pull out the dates for comparisons the previous year
nlOut = nlOut[nlOut$Day>=737669 & nlOut$Day<= 737740,]
colnames(nlOut)[4]<-'NLMetric'
colnames(nlOut)[5]<-'NLValue'
nlOut$Hour= as.numeric(nlOut$Hour)
nlOut$NLMetric= as.factor(nlOut$NLMetric)
nlOut$NLValue= as.numeric(nlOut$NLValue)

# Create a dataframe for all survey days
allDays=expand.grid(Day=737669:737740, 
                    Hour =as.numeric(8:12))
allDays=merge(allDays,nShipsPresent, by=c('Day', 'Hour'))

allDays = merge(allDays, nlOut,all.x = TRUE,
                by=c('Day','Hour'))

allDays$nShips[is.na(allDays$nShips)]=0



allDays$PRINCESSCruise = as.factor(allDays$PRINCESSCruise)

levels(allDays$NLMetric) = c('6 kHz',
                             '8 kHz',
                              '10 kHz',
                              '13 kHz',
                              '16 kHz',
                              '20 kHz',
                              '25 kHz','32 kHz','40 kHz')

# create three levels, no cruise ships, not
allDays$ShipFactor = 0
allDays$ShipFactor[which(allDays$nShips>0 & allDays$PRINCESSCruise==0)]=2
allDays$ShipFactor[which(allDays$PRINCESSCruise==1)]=1
allDays$Year =2019

ggplot(data = allDays) + 
  geom_boxplot(aes(x= NLMetric, y=NLValue, 
                   fill = as.factor(ShipFactor)))

########################################
# Load the Noise levels for 2021
#####################################
nlFolderLoc='D:\\2021CruiseShipSummaryCSV'

nlFolders = list.files(nlFolderLoc)
nlOut2021 = data.frame()

for (kk in 1:66){
  
  temp = paste(nlFolderLoc, 
               nlFolders[kk], sep = '\\')
  
  # Remove the chaff
  nldf = read.csv(temp)
  
  nldf$Hour=(do.call("rbind", strsplit(as.character(nldf$dayHr),
                                       " ", fixed = TRUE)))[,2]
  # Add useful bits
  nldf$Day = floor(nldf$MatlabTime)
  
  mdata <- melt(nldf[,c(3:14)], id=c("Day","MatlabTime","Hour"))
  mdata$Location =  as.factor("Burrard-W1")
  nlOut2021= rbind(nlOut2021, mdata)
  
  
}
rm( 'mdata', 'nldf', 'temp')



# Pull out the dates for comparisons the previous year
colnames(nlOut2021)[4]<-'NLMetric'
colnames(nlOut2021)[5]<-'NLValue'
nlOut2021$Hour= as.numeric(nlOut2021$Hour)
nlOut2021$NLMetric= as.factor(nlOut2021$NLMetric)
nlOut2021$NLValue= as.numeric(nlOut2021$NLValue)


levels(nlOut2021$NLMetric) = c('6 kHz',
                               '8 kHz',
                               '10 kHz',
                               '13 kHz',
                               '16 kHz',
                               '20 kHz',
                               '25 kHz','32 kHz','40 kHz')

nlOut2021$Year =2021
nlOut2021$ShipFactor=0
nlOut2021$PRINCESSCruise=0
nlOut2021$nShips=0



# New Combination
newOut = rbind(allDays, nlOut2021)
newOut$ShipFactor=as.factor(newOut$ShipFactor)
levels(newOut$ShipFactor)<-c('2021',
                             'Company K',
                             'Non-Company K')
p<-ggplot(data = newOut) + 
  geom_boxplot(aes(x= NLMetric, y=NLValue, 
                   fill = ShipFactor))+
 labs(fill = "")+
  theme_bw()+
  xlab('')+
  scale_fill_manual(values = c("#eab676", "#2596be", "#5ab4ac"))+
  ylab(expression('Mean 1/3 Octave SPL (dB re: 1 '*mu*"Pa)"))


# Create the figure file
jpeg("DetailedCruiseShip.jpg",
     width = 20, 
     height = 10, 
     units = "cm", res = 200)
print(p)
dev.off()
